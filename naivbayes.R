setwd("H:/Rclass/naiv")
Hebrew.stopwords <- read.csv('stopwords.csv', stringsAsFactors = FALSE) # to pull the files without changing chrs to factors
Hebrew.contact <- read.csv('contact.csv', stringsAsFactors = FALSE)

summary(Hebrew.stopwords) # to see summary of how R see the data (numbers or factors)
str(Hebrew.contact) # to see  how R see the data (numbers or factors)
summary(Hebrew.contact) 

original.contact <-Hebrew.contact 
Hebrew.contact$category <- as.factor(Hebrew.contact$category) #changing the col to factor.

Hebrew.contact$category


install.packages("ggplot2") #downlodaing a package for nice graphs.
library(ggplot2)

#check the frequency- Although we can do nothing with this in naive bayes
ggplot(Hebrew.contact, aes(category)) + geom_bar()

#installs for Work with corpus  
install.packages('tm')
library(tm)

#Create corpus
original_corpus <- Corpus(VectorSource(Hebrew.contact$text))
class(original_corpus)

original_corpus
summary(original_corpus)
str(original_corpus)
#corpus have content -our text 
#           and meta -description of the file. our meta is empty.
original_corpus[[3]][[1]]
#first argument is the index, second is the type 1- text, 2- meta


#cleaning data
clean.corpus <- tm_map(original_corpus, removePunctuation) #take of . , ! ()...
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers) #take of num
clean.corpus[[5]][[1]]

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower)) #change all letters to small letter

#take off stopwords
StopWordsHebrew <- c(Hebrew.stopwords$word) #creat new vector 
class(StopWordsHebrew) #chacking the vector type
clean.corpus <- tm_map(clean.corpus, removeWords, StopWordsHebrew) #remove words
clean.corpus[[1]][[1]]
clean.corpus[[3]][[1]]

clean.corpus <- tm_map(clean.corpus,stripWhitespace)#remove spaces
clean.corpus[[1]][[1]]

#create DTM
dtm <- DocumentTermMatrix(clean.corpus)
dim(dtm) #CHEACK- 1181 cols, 5838 rows


#create a DTM with the words that appears under 10 times
frequent.dtm <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm, 10)))
dim(frequent.dtm)

# install library
install.packages('wordcloud')
library(wordcloud)

#create colors pallet
pal <- brewer.pal(9, 'Dark2') #color pallet

wordcloud(clean.corpus[Hebrew.contact$category == 'sales'], min.freq = 5, random.order = TRUE, colors = pal)
wordcloud(clean.corpus[Hebrew.contact$category == 'support'], min.freq = 5, random.order = TRUE, colors = pal)


#frequent_dtm is matrix
#columns is files...
#rows is words
#each square have a number with the number of times that word apper in spesific file
class(frequent.dtm)
#now instead of numbers inside we want yes if the word apper and no if else..because thats enough for naive bayes

#create function to convert data to yes and no
convert_to_yes_no <- function(x){
  if(x==0) return ('no')
  return ('yes')
}

#create new metrix
yesno_matrix <- apply(frequent.dtm, MARGIN = 1:2, convert_to_yes_no)
class(yesno_matrix)

#and we can build model only for data frame so we need change this matrix to data fram
texts.df <- as.data.frame(yesno_matrix)
str(texts.df)

#insert data from original data
texts.df$category <- Hebrew.contact$category
str(texts.df)
dim(texts.df)

#split data for model 70/30
randomvector <- runif(1181) #enter the row num
filter <- randomvector > 0.3 # 70% /30%
texts.df.train <- texts.df[filter,]
texts.df.test <- texts.df[!filter,]

# install library for model
install.packages('e1071')
library(e1071)

#model
model <- naiveBayes(texts.df.train[,-268],texts.df.train$category)

#prediction
prediction <- predict(model,texts.df.test[,-268], type = 'raw')
str(prediction)
#take only the prediction where support choosen
prediction_support <- prediction[,'support']
actual <- texts.df.test$category
predicted <- prediction_support > 0.55
#so true its where support get more then 0.65 prob 

#confusion matrix 
conf_matrix <- table(predicted,actual)

#precision and recall 
recall <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[2,1])
precision <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[1,2])
#positive is supp

print(recall)
print(precision)
